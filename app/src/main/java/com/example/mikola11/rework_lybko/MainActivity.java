package com.example.mikola11.rework_lybko;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by mikola11 on 23.06.15.
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
    }
}
